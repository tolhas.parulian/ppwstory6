from django.test import TestCase
from django.urls import resolve
from .models import Status
from .views import create_status
from .forms import StatusForm
from django.apps import apps
from .apps import LandingPageConfig
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.utils import timezone
import datetime
import unittest
import time


class landing_pageUnitTest(TestCase):
    def test_landing_page_url_is_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_url_is_notexist(self):
        response = self.client.get('/tidakada/')
        self.assertEqual(response.status_code, 404)    

    def test_using_create_func(self):
        found = resolve('/')
        self.assertEqual(found.func, create_status)

    def test_landing_page_content_is_written(self):
        response = self.client.post('/', {
            'status': "sukacita",        
        })
        self.assertEqual(response.status_code, 200)
        self.assertIn("sukacita", response.content.decode('utf8'))

    def test_model_can_create_status(self):
        Status.objects.create(status = "sukacita")
        total_status = Status.objects.all().count()
        self.assertEqual(total_status, 1)        

    def test_landing_page_content_contains_greeting(self):
        response = self.client.get('/')
        self.assertIn("Halo, apa kabar?", response.content.decode('utf-8'))    

    def test_landing_page_content_is_written_error(self):
        response = self.client.post('/', {
            'status': "Nam quis nulla. Integer malesuada. In in enim a arcu imperdiet malesuada. Sed vel lectus. Donec odio urna, tempus molestie, porttitor ut, iaculis quis, sem. Phasellus rhoncus. Aenean id metus id velit ullamcorper pulvinar. Vestibulum fermentum tortor id mi. Pellentesque ipsum. Nulla non arcu lacinia walah",        
        })
        self.assertIn("Maximum characters exceeded", response.content.decode('utf8'))    

    def test_landing_page_form_is_written_error(self):
        form = StatusForm({'status': "Nam quis nulla. Integer malesuada. In in enim a arcu imperdiet malesuada. Sed vel lectus. Donec odio urna, tempus molestie, porttitor ut, iaculis quis, sem. Phasellus rhoncus. Aenean id metus id velit ullamcorper pulvinar. Vestibulum fermentum tortor id mi. Pellentesque ipsum. Nulla non arcu lacinia walah"})
        self.assertFalse(form.is_valid())
        self.assertIn("Maximum characters exceeded", form['status'].errors)    

    def test_using_landing_page_template(self):
        response = self.client.post('/')
        self.assertTemplateUsed(response, "landing_page/landing_page.html")    

    def test_landing_page_form_validation_for_blank_items(self):
        form = StatusForm(data = {'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['status'], ["Status field cannot blank"])

    def test_landing_page_apps(self):
        self.assertEqual(LandingPageConfig.name, 'landing_page')
        self.assertEqual(apps.get_app_config('landing_page').name, 'landing_page')

    def test_landing_page_valid_input_forms_to_model(self):
        form = StatusForm({'status': 'aku bersukacita'})
        self.assertTrue(form.is_valid())
        new_status = Status(status = form['status'].value())
        new_status.save()
        self.assertEqual(new_status.status, 'aku bersukacita')
        self.assertEqual(new_status.time_stamp.date(), datetime.date.today())

class landing_pageFunctionalTest(TestCase):
    def setUp(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(chrome_options = chrome_options)
        super(landing_pageFunctionalTest, self).setUp()


    def tearDown(self):
        self.selenium.quit()   
        super(landing_pageFunctionalTest, self).tearDown()

    def test_post_status(self):
        selenium = self.selenium
        selenium.get("http://localhost:8000/")
        self.assertIn('Landing page', selenium.title)

        greeting = selenium.find_element_by_id('greeting').text
        self.assertIn('Halo, apa kabar?', greeting)

        status_box = selenium.find_element_by_name('status')
        status_box.send_keys('Coba Coba')
        status_box.submit()
        time.sleep(3)

        self.assertIn('Coba Coba', selenium.page_source)










    
