from .models import Status
from django import forms

class StatusForm(forms.ModelForm):
    status = forms.CharField(required = False, label = 'Share your status here!', widget=forms.Textarea(
    	attrs={
    		'class': 'md-textarea form-control',
    		'placeholder': "Status max. 300 characters",
            'rows': '3'
    	}
    ))

    class Meta:
        model = Status
        fields = ['status']

    def clean_status(self):
        status = self.cleaned_data['status']
        if len(status) == 0:
            self.add_error('status', 'Status field cannot blank')
        if len(status) > 300:
            self.add_error('status', 'Maximum characters exceeded')
