from django.db import models

class Status(models.Model):
    status = models.TextField(max_length = 300)
    time_stamp = models.DateTimeField(null=True, blank=True, auto_now_add=True)