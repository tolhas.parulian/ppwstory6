from django.urls import path
from .views import create_status

app_name = "landing_page"

urlpatterns = [
    path('', create_status, name='create_status')
]