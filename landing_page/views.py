from django.shortcuts import render
from django.http import HttpResponse
from .forms import StatusForm
from .models import Status

def create_status(request):
    response = {}
    form = StatusForm(request.POST or None)
    status_datas = Status.objects.all().values().order_by('time_stamp')
    response ['status_datas'] = status_datas
    if request.method == 'POST' and form.is_valid():
        new_status = Status(status = request.POST['status'])
        new_status.save()
        response['StatusForm'] = StatusForm
        return render(request, 'landing_page/landing_page.html', response)
    response['StatusForm'] = form
    return render(request, 'landing_page/landing_page.html', response)

    
